# fedora-livecd-sway.ks
#
# Description:
# - Fedora Live Spin with the tiling window manager sway
#
# Maintainer(s): 
# - Firstname Last   <example@fedoraproject.org>


repo --name=sway-extras --baseurl=https://download.copr.fedorainfracloud.org/results/alebastr/sway-extras/fedora-$releasever-$basearch/ --install

%packages
sway
greetd
greetd-tuigreet

# unlock default keyring. FIXME: Should probably be done in comps
gnome-keyring-pam
# Admin tools are handy to have
@admin-tools
wget
# Better more popular browser
system-config-printer

# save some space
-autofs
-acpid
-gimp-help
-desktop-backgrounds-basic
-aspell-*                   # dictionaries are big

%end
