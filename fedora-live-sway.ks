# fedora-livecd-sway.ks
#
# Description:
# - Fedora Live Spin with the tiling window manager sway
#
# Maintainer(s):
# - First Last       <example@fedoraproject.org>

%include fedora-live-base.ks
%include fedora-live-minimization.ks
%include fedora-sway-common.ks

%post
# xfce configuration

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/sway
DISPLAYMANAGER=/usr/bin/greetd
EOF

cat >> /etc/rc.d/init.d/livesys << EOF

# deactivate xfconf-migration (#683161)
rm -f /etc/xdg/autostart/xfconf-migration-4.6.desktop || :

# set up greetd autologin
sed -i 's/^command.*=.*".*"/command = "tuigreet --cmd sway"' /etc/greetd/config.toml
echo '[initial session]' >> /etc/greetd/config.toml
echo 'command = "sway"' >> /etc/greetd/config.toml
echo 'user = "liveuser"' >> /etc/greetd/config.toml

# Show harddisk install on the desktop
#sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
#mkdir /home/liveuser/Desktop

# this goes at the end after all other changes.
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

# setting the wallpaper
#echo "/usr/bin/feh --bg-scale /usr/share/backgrounds/default.png" >> /home/liveuser/.profile

# echoing type liveinst to start the installer
echo "echo 'Please type liveinst and press Enter to start the installer'" >> /home/liveuser/.bashrc

# fixing the installer non opening bug
echo "xhost si:localuser:root" >> /home/liveuser/.profile

EOF

%end

